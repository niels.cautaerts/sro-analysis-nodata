# Analysis of SRO data in emd file

## Summary
In this jupyter notebook I use TEMMETA and hyperspy to analyze STEM-EDX maps in refractory alloy (Ti-Zr-Hf-Ta). The goal is to find out whether HAADF image intensity was correlated with any EDX intensity. To do this we run peak-finding on the HAADF image, fit gaussians to the image and sum the HAADF intensity over the atomic columns. We then use the refined peaks to get an integrated EDX intensity for each of the above elements. The images, element maps and atom-column-integrated values are compared in various plots, which shows clearly that Ta clusters and correlates with HAADF intensity.

## Running it

Clone the repository and create a virtual environment that has TEMMETA installed in it, then open the .ipynb file with jupyter notebook.
